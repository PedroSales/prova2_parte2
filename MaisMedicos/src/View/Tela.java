package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.ScrollPane;
import java.awt.Choice;

import javax.swing.JPopupMenu;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuBar;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;

import java.awt.Button;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JProgressBar;
import javax.swing.JInternalFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLayeredPane;
import javax.swing.JDesktopPane;

import Controllers.ControleConsulta;
import Controllers.ControleUsuario;
import Models.Consulta;
import Models.Medico;
import Models.Paciente;

public class Tela extends JFrame {

	private JPanel contentPane;
	private JTextField textNomeMedico;
	private JTextField textCRMMedico;
	private JTextField textEspecialidadeMedico;
	private JTextField textNomePaciente;
	private JTextField textCadastroPaciente;
	private JTextField textPlanoPaciente;
	private JTextField textLoginMedico;
	private JTextField textCRMedico;

	private ControleUsuario umControle = new ControleUsuario();
	private ControleConsulta umControleConsulta = new ControleConsulta();
	private JTextField textPacienteConsulta;
	private JTextField textDataConsulta;
	private JTextField textHoraConsulta;
	private JTextField textNomePacMed;
	private JTextField textMedicamento;
	private JTextField textLogMedMed;
	private JTextField textCRMedMed;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tela frame = new Tela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tela() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 799, 560);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		
		
		JLabel lblNome = new JLabel("Nome");
		
		textNomeMedico = new JTextField();
		textNomeMedico.setColumns(10);
		
		JLabel lblCrm = new JLabel("CRM");
		
		textCRMMedico = new JTextField();
		textCRMMedico.setColumns(10);
		
		JLabel lblEspecialidade = new JLabel("Especialidade");
		
		textEspecialidadeMedico = new JTextField();
		textEspecialidadeMedico.setColumns(10);
		
		JLabel lblCadastroPaciente = new JLabel("Cadastro Paciente");
		lblCadastroPaciente.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel label = new JLabel("Nome");
		
		textNomePaciente = new JTextField();
		textNomePaciente.setColumns(10);
		
		JLabel lblCadastro = new JLabel("Cadastro");
		
		JLabel lblPlanoDeSaude = new JLabel("Plano de Saude");
		
		textCadastroPaciente = new JTextField();
		textCadastroPaciente.setColumns(10);
		
		textPlanoPaciente = new JTextField();
		textPlanoPaciente.setColumns(10);
		
		JButton btnCadastrarMdico = new JButton("Cadastrar M\u00E9dico");
		btnCadastrarMdico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Medico umMedico = new Medico(textNomeMedico.getText() , textCRMMedico.getText());
				umMedico.setEspecialidade(textEspecialidadeMedico.getText());
			
				umControle.adicionarMedico(umMedico);
				JOptionPane.showMessageDialog(null, "Medico Cadastrado!");
			}
		});
		
		JButton btnCadastrarPaciente = new JButton("Cadastrar Paciente");
		btnCadastrarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Paciente umPaciente = new Paciente(textNomePaciente.getText());
				umPaciente.setCadastroPaciente(textCadastroPaciente.getText());
				umPaciente.setPlanoDeSaude(textPlanoPaciente.getText());
			
				umControle.adicionarPaciente(umPaciente);
				JOptionPane.showMessageDialog(null, "Paciente Cadastrado!");
				
			}
		});
		
		JLabel lblConsulta = new JLabel("Consulta");
		lblConsulta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel lblLogin = new JLabel("Login M\u00E9dico");
		
		JLabel lblCrm_1 = new JLabel("CRM");
		
		
		
		JLabel label_1 = new JLabel("Cadastro Medicos");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		textLoginMedico = new JTextField();
		textLoginMedico.setColumns(10);
		
		textCRMedico = new JTextField();
		textCRMedico.setColumns(10);
		
		JLabel lblNomePaciente = new JLabel("Nome Paciente");
		
		JLabel lblData = new JLabel("Data");
		
		JLabel lblHora = new JLabel("Hora");
		
		textPacienteConsulta = new JTextField();
		textPacienteConsulta.setColumns(10);
		
		textDataConsulta = new JTextField();
		textDataConsulta.setColumns(10);
		
		textHoraConsulta = new JTextField();
		textHoraConsulta.setColumns(10);
		
		JButton btnMarcarConsulta = new JButton("Marcar Consulta");
		btnMarcarConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Medico umMedico = umControle.buscarMedico(textLoginMedico.getText());
				if(umMedico != null && umMedico.getCRM() == textCRMedico.getText()){
				
					umControleConsulta.MarcaConsulta(textPacienteConsulta.getText(), textDataConsulta.getText(), textHoraConsulta.getText(), umMedico);
					
					JOptionPane.showMessageDialog(null, "Consulta Marcada");
				}
				
				else{JOptionPane.showMessageDialog(null, "N�o foi poss�vel marcar a consulta");
				}
				}
		});
		
		JButton btnDesmarcarConsulta = new JButton("Desmarcar Consulta");
		btnDesmarcarConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
					Paciente umPaciente = umControleConsulta.BuscaPaciente(textPacienteConsulta.getText());
					if(umPaciente != null){
					umControleConsulta.CancelaConsulta(textPacienteConsulta.getText());
					JOptionPane.showMessageDialog(null, "Consulta Desmarcada");
					}
					else{
						JOptionPane.showMessageDialog(null, "Consulta n�o encontrada");
					}
				
			}
		});
		
		JLabel lblMedicamentos = new JLabel("Medicamentos");
		lblMedicamentos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel NomePacienteMed = new JLabel("Nome Paciente");
		
		textNomePacMed = new JTextField();
		textNomePacMed.setText("");
		textNomePacMed.setColumns(10);
		
		JLabel lblMedicamento = new JLabel("Medicamento");
		
		textMedicamento = new JTextField();
		textMedicamento.setColumns(10);
		
		JLabel lblLoginMdico = new JLabel("Login M\u00E9dico");
		
		JLabel lblCrm_2 = new JLabel("CRM");
		
		textLogMedMed = new JTextField();
		textLogMedMed.setColumns(10);
		
		textCRMedMed = new JTextField();
		textCRMedMed.setColumns(10);
		
		JButton btnPrescreverMedicamento = new JButton("Prescrever Medicamento");
		btnPrescreverMedicamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Medico umMedico = umControle.buscarMedico(textLogMedMed.getText());
				if(umMedico != null && umMedico.getCRM() == textCRMedMed.getText()){  
				umControleConsulta.PrescreveMedicamento(textMedicamento.getText(), textNomePacMed.getText());
				JOptionPane.showMessageDialog(null, "Medicamento Prescrito!");
				}
				else{
					JOptionPane.showMessageDialog(null, "N�o foi poss�vel prescrever o medicamento");
				}
			}
		});
		
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(btnMarcarConsulta)
					.addGap(18)
					.addComponent(btnDesmarcarConsulta)
					.addContainerGap(515, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(10)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(lblEspecialidade)
										.addComponent(lblNome)
										.addComponent(lblCrm))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(textNomeMedico, GroupLayout.PREFERRED_SIZE, 229, GroupLayout.PREFERRED_SIZE)
										.addComponent(textCRMMedico, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
										.addComponent(textEspecialidadeMedico, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
								.addComponent(btnCadastrarMdico)
								.addComponent(lblConsulta)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblCrm_1)
								.addComponent(lblNomePaciente)
								.addComponent(lblData)
								.addComponent(lblHora)
								.addComponent(lblLogin))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(textLoginMedico, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(textDataConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(textHoraConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(textCRMedico, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(textPacienteConsulta, GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE)))))
					.addGap(32)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCadastro)
						.addComponent(lblPlanoDeSaude)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(8)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(NomePacienteMed, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMedicamento)
								.addComponent(lblLoginMdico)
								.addComponent(lblCrm_2)))
						.addComponent(label, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(textNomePaciente, GroupLayout.PREFERRED_SIZE, 229, GroupLayout.PREFERRED_SIZE)
						.addComponent(textCadastroPaciente, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
						.addComponent(textPlanoPaciente, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
						.addComponent(textMedicamento, 144, 144, 144)
						.addComponent(textNomePacMed, GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
						.addComponent(textLogMedMed, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
						.addComponent(textCRMedMed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(374)
					.addComponent(btnPrescreverMedicamento)
					.addGap(248))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(366)
					.addComponent(btnCadastrarPaciente)
					.addGap(282))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(458)
					.addComponent(lblCadastroPaciente)
					.addContainerGap(190, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(366)
					.addComponent(lblMedicamentos)
					.addGap(307))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCadastroPaciente))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(textNomeMedico, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label)
						.addComponent(textNomePaciente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCrm)
						.addComponent(textCRMMedico, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCadastro)
						.addComponent(textCadastroPaciente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEspecialidade)
						.addComponent(textEspecialidadeMedico, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPlanoDeSaude)
						.addComponent(textPlanoPaciente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCadastrarMdico)
						.addComponent(btnCadastrarPaciente))
					.addGap(44)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblConsulta)
						.addComponent(lblMedicamentos))
					.addGap(42)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNomePaciente)
						.addComponent(textPacienteConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(NomePacienteMed, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
						.addComponent(textNomePacMed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblData)
						.addComponent(textDataConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblMedicamento)
						.addComponent(textMedicamento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblHora)
						.addComponent(textHoraConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(24)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLogin)
						.addComponent(lblLoginMdico)
						.addComponent(textLoginMedico, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textLogMedMed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCrm_1)
						.addComponent(textCRMedico, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCrm_2)
						.addComponent(textCRMedMed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnMarcarConsulta)
						.addComponent(btnDesmarcarConsulta)
						.addComponent(btnPrescreverMedicamento))
					.addGap(16))
		);
		contentPane.setLayout(gl_contentPane);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	public ControleConsulta getUmControleConsulta() {
		return umControleConsulta;
	}

	public void setUmControleConsulta(ControleConsulta umControleConsulta) {
		this.umControleConsulta = umControleConsulta;
	}

	
}
